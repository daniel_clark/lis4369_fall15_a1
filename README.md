lis4369 Daniel Clark

1. git init- Create an empty Git repository or reinitialize an existing one

2. git status- Show the working tree status

3. git add- Add file contents to the index

4. git commit- Record changes to the repository

5. git push- Update remote refs along with associated objects

6. git pull- Fetch from and integrate with another repository or a local branch

7. git-request-pull - Generates a summary of pending changes


### VS ScreenShot
![vs.png](images/vs.png)

### Link
[git tutorial](https://bitbucket.org/daniel_clark/bitbucketstationlocations)

[personal website](https://www.arvixe.com/billing.php)(http://www.dclarkh.net)

